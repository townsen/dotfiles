# Sourced in login shells (THIRD).
# It should contain commands that should be executed only in login shells
#
# This is not the place for alias definitions, options, environment variable settings, etc.
# As a general rule, it should not change the shell environment at all.
# Rather, it should be used to set the terminal type and run a series of external commands (fortune, msgs, etc).

# Setup the prompt (after PATH is configured as we need git)

[ -f ~/.zsh_prompt ] && . ~/.zsh_prompt

# Setup MacOS specifics

if [[ ! -z "${MACOS_VERSION}" ]] ; then
    [ -f /etc/zprofile ] && echo "Rogue /etc/zprofile has returned: PATH possibly incorrect!"
    # export PATH="/Applications/Postgres.app/Contents/MacOS/bin:$PATH"
    # export DATABASE_URL=postgres://localhost/townsen
    # export SDKROOT=`xcrun --show-sdk-path`
    export HOMEBREW_GITHUB_API_TOKEN=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

fi

autoload -U +X compinit && compinit
autoload -U +X bashcompinit && bashcompinit

# If using chruby then load it, and enable autoswitch with .ruby-version

chruby_share=/usr/local/share

if [ -f $chruby_share/chruby/chruby.sh ]; then
    source $chruby_share/chruby/chruby.sh
    source $chruby_share/chruby/auto.sh
fi

if [[ ! -z "${MACOS_VERSION}" ]] ; then
    LUNCHY=`gem which lunchy 2>/dev/null`
    if [ -n "$LUNCHY" ]; then
        LUNCHY_DIR=$(dirname $LUNCHY)/../extras
        if [ -f $LUNCHY_DIR/lunchy-completion.zsh ]; then
         . $LUNCHY_DIR/lunchy-completion.zsh
        fi
    fi
fi

# When run inside a container and no SSH then start an agent

if [ -z "$SSH_AUTH_SOCK" ]; then
  if type -p ssh-agent; then
    eval $(ssh-agent)
  fi
fi

# Setup Git subrepo under Linux

if [ -f ~/.git-subrepo/.rc ]; then
    source ~/.git-subrepo/.rc
fi

# vim:ts=4:sw=4
