#!/bin/bash
#
#set -e

uid=1606
gid=1606

apt-get update
apt-get -y install zsh ruby git
if ! adduser --uid $uid --shell /bin/zsh  --gecos "Nick Townsend" townsen; then
  echo "User townsen already exists, setting UID = ${uid} and GID..."
  usermod -u ${uid} -s /bin/zsh townsen
fi
cat > /etc/sudoers.d/townsen <<SUDO
townsen ALL=(ALL:ALL) NOPASSWD: ALL
Defaults env_keep += SSH_AUTH_SOCK
SUDO
chmod 440 /etc/sudoers.d/townsen
echo "Added townsen to sudoers.d"
echo "Done"
