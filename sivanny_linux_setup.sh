#!/bin/bash
#
set -e

adduser --uid 1211 --shell /bin/bash  --gecos "Sivanny Selvakumar" sivanny
cat > /etc/sudoers.d/sivanny <<SUDO
sivanny ALL=(ALL:ALL) NOPASSWD: ALL
Defaults env_keep += SSH_AUTH_SOCK
SUDO
chmod 440 /etc/sudoers.d/sivanny
