# Sourced in interactive shells (SECOND).
# It should contain commands to set up aliases, functions, options, key bindings, etc.
#
# Put things in here that would not be inherited by subshells automatically:
# - aliases and functions
# - variable settings that should not be visible outside the shell
#
# Do not put regular variables in here!

setopt AUTO_CD

# Command line history
#
HISTFILE=${ZDOTDIR:-$HOME}/.zsh_history
SAVEHIST=2000
HISTSIZE=2000
setopt EXTENDED_HISTORY
setopt APPEND_HISTORY
setopt HIST_EXPIRE_DUPS_FIRST
setopt HIST_IGNORE_DUPS
setopt HIST_IGNORE_ALL_DUPS
setopt HIST_FIND_NO_DUPS
setopt HIST_REDUCE_BLANKS
setopt HIST_VERIFY

unsetopt CORRECT
unsetopt CORRECT_ALL
setopt NONOMATCH

bindkey -v
bindkey '∆' history-beginning-search-forward
bindkey '˚' history-beginning-search-backward

case $TERM in
    xterm*)
        precmd () {print -Pn "\e]0;%n@%m: %~\a"}
        ;;
esac

# Attach to existing tmux on remote session, create new if one does not exist
# or just start bash if there is no tmux available

function ssht(){
  ssh $* -t 'tmux -u a || tmux -u || /bin/bash'
}

function running_in_docker() {
    if [ -f /proc/self/cgroup ]; then
        awk -F/ '$2 == "docker"' /proc/self/cgroup | read
    else
        false
    fi
}

alias mvim="open -a /Applications/MacVim.app"
alias vmrun="/Applications/VMware\ Fusion.app/Contents/Library/vmrun"
alias cdate='date "+%a, %d %b %Y %T %z"'
alias glog='git log --oneline --graph --all --decorate'
alias fixvenv='perl -spi.bak -e "s/#!\/bin\/false/#!\/usr\/bin\/false/" post* pre* initialize'
# For the Raspberry Pi
alias pimodel='cat /sys/firmware/devicetree/base/model'
alias picon='screen /dev/tty.usbserial 115200'
alias pirecon='screen -rD'
alias pitemp="paste <(cat /sys/class/thermal/thermal_zone*/type) <(cat /sys/class/thermal/thermal_zone*/temp) | column -s $'\t' -t | sed 's/\(.\)..$/.\1°C/'"

# Adapted from: http://timstaley.co.uk/posts/making-git-and-jupyter-notebooks-play-nice/

function fakegato {
    jq '.history[]|(.time?|todate),.temp?' "$*"
}

function nbstrip {
    jq --indent 1 \
    '(.cells[] | select(has("outputs")) | .outputs) = []
    | (.cells[] | select(has("execution_count")) | .execution_count) = null
    | .metadata = {"language_info": {"name": "python", "pygments_lexer": "ipython3"}}
    | .cells[].metadata = {}' "$*"
}

function nbstrip_cwd {
    for nbfile in *.ipynb; do
        nbstrip "${nbfile}" > "${nbfile}.new"
        mv -f "${nbfile}.new" "${nbfile}"
    done
    unset nbfile
}

fpath=(~/.zsh $fpath)
fpath=(~/.zsh/completion $fpath)
autoload -U compinit
compinit

# vim:ts=4:sw=4

