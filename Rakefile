# Rakefile to setup Nix dotfiles and utilities
#
homedir = ENV['HOME']
kernel, ARCH = `uname -ms`.split rescue raise "Can't determine OS type: #{$!}"

OSNAME = case kernel
	  when 'Darwin' then 'macOS'
	  when 'Linux' then 'Linux'
	  else 'Unknown'
	 end

desc "Setup command line environment (dotfiles, brew, vim, utils)"
task :default => [:config, :brew, :utils, :vim]

configs = %w{
  agignore
  config/flake8
  config/paths/paths.d/00-homebin
  gemrc
  gitconfig
  gitattributes
  gitignore
  ideavimrc
  inputrc
  tmux.conf
  bundle/config
  vimrc
  zlogin
  zsh_prompt
  zshrc
  zshenv
}

# https://github.com/yb66/path_helper
# When running on Linux there is no /etc/paths so create one to make it look like MacOS
#
desc "Path Helper"
task :path_helper do
  next if File.exist? '/usr/local/libexec/path_helper'
  sh "sudo mkdir -p /usr/local/libexec"
  sh "sudo curl -s -z /usr/local/libexec/path_helper -LSso /usr/local/libexec/path_helper https://raw.githubusercontent.com/yb66/path_helper/master/exe/path_helper"
  sh "sudo chmod +x /usr/local/libexec/path_helper"
  sh "sudo rm -f /etc/zprofile" # Contains the call to Apple's flaky path_helper
  if OSNAME == 'Linux' and not File.exist?('/etc/paths')
    sh "sudo install -o #{Process.uid} -g #{Process.gid} -m 664 /dev/null /etc/paths "
    File.open('/etc/paths','w') do |f|
      f.write("/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin\n")
    end
  end
end

desc "Link configuration files into #{homedir}"
task :config => :path_helper do
  configs.each do |config|
    dirpart = File.dirname(config)
    mkdir_p "#{homedir}/.#{dirpart}" if dirpart != '.'
    ln_sf File.absolute_path(config), "#{homedir}/.#{config}"
  end
end

desc "Setup VIM"
task :vim do
  if OSNAME == 'macOS'
    system("brew list macvim > /dev/null") ?
      sh("brew upgrade macvim") : sh("brew install macvim")
  else
    sh "sudo apt-get -y install vim"
  end
  mkdir_p %w{autoload}.map{|dir| "#{homedir}/.vim/#{dir}"}
  ln_sf File.absolute_path("vimrc"), "#{homedir}/.vimrc"
  sh "curl -fLo ~/.vim/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim"
  sh "npm install eslint eslint-config-strongloop" rescue nil
end

desc "Setup Git with subrepo and LFS"
task :git => :brew do
  if OSNAME == 'macOS'
    sh "brew install git git-subrepo git-lfs git-secrets" # rescue nil
  else
    sh "sudo apt-get install -y git git-lfs"
    sh "git clone https://github.com/ingydotnet/git-subrepo #{homedir}/.git-subrepo"
  end
end

# Install ruby-install and chruby
desc "Ruby Setup"
task :ruby => :brew do
  # Use bundler with the system ruby
  begin
    require 'bundler'
  rescue LoadError 
    sh "sudo gem install bundler"
  end
  if OSNAME == 'macOS'
    sh "brew install ruby-install chruby" rescue nil
  else
    cd "/tmp" do
      ruby_install_version = '0.7.0'
      sh <<-RUBY_INSTALL
	wget -O ruby-install-#{ruby_install_version}.tar.gz \
	https://github.com/postmodern/ruby-install/archive/v#{ruby_install_version}.tar.gz &&
	tar -xzf ruby-install-#{ruby_install_version}.tar.gz &&
	cd ruby-install-#{ruby_install_version}/ &&
	sudo make install
	RUBY_INSTALL
      chruby_version = '0.3.9'
    sh <<-CHRUBY_INSTALL
	wget -O chruby-#{chruby_version}.tar.gz \
	https://github.com/postmodern/chruby/archive/v#{chruby_version}.tar.gz &&
	tar -xzf chruby-#{chruby_version}.tar.gz &&
	cd chruby-0.3.9/ &&
	sudo make install
	CHRUBY_INSTALL
    end
  end
  sh "ruby-install ruby"
end

desc "Install brew"
task :brew => :path_helper do
  if OSNAME == 'macOS'
    basepath = ARCH == 'arm64'? '/opt/homebrew': '/usr/local'
    if File.exist?("#{basepath}/bin/brew")
      puts "Brew already installed at #{basepath}"
    else
      sh '/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install.sh)"'
      File.open("#{homedir}/.config/paths/paths.d/01-brew",'w') do |f| 
	%w{ bin sbin }.each { |d| f.puts("#{basepath}/#{d}") }
      end
    end
  else
    puts "Skipping brew install on #{RUBY_PLATFORM}"
  end
end

desc "Install utilities"
task :utils do
    if OSNAME == 'macOS'
      sh "brew install ag awscli jq"
    else
      sh "sudo apt-get -y install curl git jq silversearcher-ag zsh"
    end
end

desc "Docker"
task :docker do
  if OSNAME == 'macOS'
    unless Dir.exist? "/Applications/Docker.app"
      puts "You need to install Docker for Mac! Follow the link then run this again afterwards..."
      sleep 1
      sh "open https://hub.docker.com/editions/community/docker-ce-desktop-mac"
      exit
    end
    puts "Setting up bash command completion for Docker..."
    files = %w{ docker docker-machine docker-compose }
    cd "/usr/local/etc/bash_completion.d" do
      files.each do |f|
	file = f+".bash-completion"
	ln_sf "/Applications/Docker.app/Contents/Resources/etc/#{file}", file
      end
    end
  else
    sh "sudo apt-get -y install docker-ce"
  end
end

desc "Install MiniConda (can now be done with brew install miniforge)"
task :miniconda do
  if File.executable?("#{ENV['HOME']}/miniconda3/bin/conda")
    puts "MiniConda is already installed"
  else
    sh <<-INSTALL
	  cd /tmp
	  curl -L -O https://repo.anaconda.com/miniconda/Miniconda3-latest-#{OSNAME}-#{ARCH}.sh
	  bash Miniconda3-latest-MacOSX-#{ARCH}.sh
	  INSTALL
  end
end

# vim: ts=8 sw=2 sts=2 noet
