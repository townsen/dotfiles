" Nick's VIM settings
"
" After first run use the :PlugInstall command to actually install them!
"
" Plugins will be downloaded under the specified directory.
"
let g:plug_shallow=1
let data_dir = has('nvim') ? stdpath('data') . '/site' : '~/.vim'
if empty(glob(data_dir . '/autoload/plug.vim'))
  silent execute '!curl -fLo '.data_dir.'/autoload/plug.vim --create-dirs  https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'
  autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

call plug#begin(has('nvim') ? stdpath('data') . '/plugged' : '~/.vim/plugged')

" Declare the list of plugins.
Plug 'tpope/vim-fugitive'
Plug 'keith/swift'
Plug 'nvie/vim-flake8'
Plug 'dense-analysis/ale'
Plug 'skywind3000/asyncrun.vim'

" List ends here. Plugins become visible to Vim after this call.
call plug#end()

" Basic Settings --- {{{
filetype plugin indent on
highlight User1 guifg=green guibg=darkslategray term=inverse,bold cterm=bold ctermfg=green ctermbg=black
" For ALE errors
highlight SpellBad ctermfg=white ctermbg=darkblue
runtime macros/matchit.vim
syntax on

if has('gui_running')
"  set guifont=Lucida_Console:h13
  set guifont=Menlo\ Regular:h12
endif
" }}}

" Set variables --- {{{
set autowrite
set encoding=utf-8
set laststatus=2
set modelines=5
set nocompatible
set path=,Sources/**,Tests/**,src/**,tst/**,test/**
set regexpengine=0 " stops 100% cpu editing TypeScript files on OSX
set shell=/bin/bash
set sta
set statusline=%<%f%h%m%r                       " File, help, modified, readonly
set statusline+=%=                              " Switch to RHS
set statusline+=%1*%{fugitive#statusline()}%*\  " Git info
set statusline+=0x%B\ \                         " Hex value of character under cursor
set statusline+=L\ %l,\ C\ %c%V\ %P
set title
set ts=8 sw=2 sts=2 et
set virtualedit=block
set visualbell
set wildmenu
set wildmode=list:longest
" }}}

" Key mappings --- {{{
"
" With xterm-256color the VIM termcap lacks definitions for some terminal keys
" t_F3, t_F4 and t_F5 in particular. Use 'set termcap' to see this.
" This can easily be fixed for F13 as shown but the macOS terminal defaults
" for F14 and F15 are mapped to sequences used by vim terminal keys for <Undo>
" and <Help> and these can't be overridden, so define new escape sequences for
" those both here and in macOS terminal
"
if $TERM == "xterm-256color"
  set <F13>=[25~
  set <F14>=[1;2Q
  set <F15>=[1;2R
endif

" ESC is hard to find on some MacBooks
noremap ;; <Esc>
inoremap ;; <Esc>
cnoremap ;; <Esc>

let mapleader = ","

nnoremap <leader>ev :split $MYVIMRC<cr>
nnoremap <leader>sv :source $MYVIMRC<cr>
nnoremap gf :tabe <cfile><CR>

set listchars=tab:>-,trail:·,eol:$
nnoremap <silent> <leader>s :set nolist!<CR>

" Add a changelog timestamp
nnoremap <leader>ts "= strftime(" %a, %d %b %Y %T %z")<CR>p

" Format JSON
nnoremap <leader>fj :%! cat % \| ruby -e "require 'json'; puts JSON.pretty_generate(JSON.parse(STDIN.read))"<CR>
" Format and sort keys in JSON
nnoremap <leader>fsj :%! cat % \| ruby -e "
  \ require 'json';
  \ hash = JSON.parse(STDIN.read);
  \ def deep_sort(hash);
  \   hash.sort.map { \|(k, v)\|;
  \     case v;
  \     when Hash;
  \       [k, deep_sort(v)];
  \     when Array;
  \       [k, v.sort_by(&:to_s)];
  \     else;
  \       [k, v];
  \     end;
  \   }.to_h;
  \ end;
  \ puts JSON.pretty_generate(deep_sort(hash))"<CR><CR>

" }}}

" Use The Silver Searcher for :grep --- {{{
if executable('ag')
  set grepprg=ag\ --vimgrep\ \'$*\'
  set grepformat=%f:%l:%c:%m
endif
" }}}

" Open large files > 10 MB --- {{{
"
let g:LargeFile = 10 * 1024 * 1024
augroup LargeFile
  " files with filesize too large are recognized too (getfsize = -2)
  autocmd BufReadPre * let f=getfsize(expand("<afile>")) | if f > g:LargeFile || f == -2 | call LargeFile() | endif
augroup END
function! LargeFile()
  set eventignore+=FileType " no syntax highlighting etc
  setlocal bufhidden=unload " save memory when other file is viewed
  setlocal undolevels=-1 " no undo possible
  setlocal foldmethod=manual
  setlocal noswapfile
endfunction
" }}}

" File type setup --- {{{
"
augroup filetypes
  autocmd!
  autocmd BufNewFile,BufRead *_spec.rb compiler rspec
  autocmd BufWritePre *.rb,*.coffee,*.md,*.rake,*.clj,*.js,*.jsx :%s/\s\+$//e  " remove trailing whitespaces
  autocmd FileType c setlocal tw=100 cc=100
  autocmd FileType cpp setlocal tw=100 cc=100
  autocmd FileType css,scss,sass setlocal iskeyword+=-    " allow stylesheets to autocomplete hyphenated words
  autocmd FileType gitcommit setlocal textwidth=72
  autocmd FileType java setlocal sw=4 sts=4 et tw=100 makeprg=javac
  autocmd FileType json setlocal ts=2 sw=2 sts=2 et tw=100
  autocmd FileType markdown setlocal textwidth=90
  autocmd FileType python setlocal sw=4 sts=4 et tw=100 cc=100 | noremap <buffer> <F12> :call Flake8()<CR>
  autocmd FileType ruby,eruby,yaml setlocal iskeyword+=?  " make ?s part of words
  autocmd FileType swift setlocal sw=4 sts=4 et tw=120 makeprg='swift-build'
  autocmd FileType vim setlocal foldmethod=marker foldlevel=1
augroup END
" }}}

nmap <silent> <leader>aj :ALENext<cr>
nmap <silent> <leader>ak :ALEPrevious<cr>

" Stuff for asyncrun --- {{{
"
augroup vimrc
    autocmd QuickFixCmdPost * call asyncrun#quickfix_toggle(8, 1)
augroup END

let g:asyncrun_exit = "silent call system('afplay /System/Library/PrivateFrameworks/ToneLibrary.framework/Versions/A/Resources/AlertTones/Modern/sms_alert_complete.caf &')"

command! -bang -nargs=* -complete=file Make AsyncRun -save=2 -program=make @ <args>

nnoremap <F8> :Make<CR>
inoremap <F8> <ESC>:Make<CR>
nnoremap <F7> :cp<CR>
inoremap <F7> <ESC>:cp<CR>
nnoremap <F9> :cn<CR>
inoremap <F9> <ESC>:cn<CR>
nnoremap <F10> :cclose<CR>
inoremap <F10> <ESC>:cclose<CR>

" }}}
