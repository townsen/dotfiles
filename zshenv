# Sourced FIRST on all invocations of the shell, unless the -f option is set.
# It should contain commands to set the command search path, plus other important environment variables.
# It should NOT contain commands that produce output or assume the shell is attached to a tty.
#
# The startup sequence is (taken from the manpage):
#   /etc/zshenv (cannot be changed but doesn't exist on Big Sur 11.1)
#   ~/.zshenv (This file)
# If it's a login shell:
#   /etc/zprofile (exists on Big Sur 11.1 and sets a faulty path, see below)
#   ~/.zprofile
# If it is an interactive shell
#   /etc/zshrc
#   ~/.zshrc
# If it's a login shell:
#   /etc/zlogin
#   ~/.zlogin
#
# On MacOS the default path is set by /usr/libexec/path_helper (called from /etc/zprofile)
# using '/etc/paths' and manpath in '/etc/manpaths'. This is borked by the inability to
# prepend your own bin dir to the path. See: https://openradar.appspot.com/14630658
#
# Fix this by DELETING /etc/zprofile and using https://github.com/yb66/path_helper as follows:
#
[[ $(uname) == "Darwin" ]] && export MACOS_VERSION=$(sw_vers -productVersion)

export PATH=$(/usr/local/libexec/path_helper --config --no-lib -p "" 2>/dev/null)
export MANPATH=$(/usr/local/libexec/path_helper --config -m)
export EDITOR=/usr/bin/vim
export LESS=-R
export RI=-fansi

umask 002 # So that we can use groups etc.
