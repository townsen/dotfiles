# Site Services Setup

Each home has a set of services that run in containers as a Compose project

# Running in a Linux VM

This seemed like a good idea but randomly the VM won't start and there's no easy way to
debug why, resulting in using Time Machine to restore the image and re-applying whatever
caused it to fail. So this will be deprecated to run natively on Docker in MacOS. This
does not allow _macvlan_ networking so port mapping has to be used.

## Moving containers to MacOS

DNS Updater: no ports required
NTP Server: udp/123 exposed
Gollum: tcp/80
Zigbee2MQTT: two instances 8081 for the main, 8082 for the shed
Mosquitto: maps all ports.

The Homebridge and Scrypted containers are not used as they require host networking.
They are installed natively also on the Mac.

## Additional Steps

### Homebridge

    * Add 'mosquitto' to the `/etc/hosts` file to allow the Homebridge plugin to reach the
      server
    * Create a Python virtual environment to install `rpgio`

    python3 -m venv ~/.homebridge/.venv
    source ~/.homebridge/.venv/bin/activate
    python3 -m pip install rgpio

    * Update the `/Library/LaunchDaemons/com.homebridge.server.plist` file by
      prepending `/Users/townsen/.homebridge/.venv/bin` to the PATH and adding:

            <key>VIRTUAL_ENV</key>
                <string>/Users/townsen/.homebridge/.venv</string>

### Scrypted

    The database for scrypted is stored at the path `.scrypted/volume` so create a
symbolic link from the mirrored service volume:

    ln -sf /Volumes/Mirror/Services/scrypted ~/.scrypted/volume

Then restart scrypted:

    launchctl unload ~/Library/LaunchAgents/app.scrypted.server.plist
    launchctl load ~/Library/LaunchAgents/app.scrypted.server.plist

# Deprecated

## Starting a Linux VM when logging in

Edit the net.sendium.livm.plist file to start the required VM with the correct options
then copy the file to ~/Library/LaunchAgents/

## Running multiple containers with an individual IP per container

In order that each of the web services can use the default ports (ie. 80/443) each
container must be allocated its own IP address on the local network. To do this create a
macvlan network that occupies a subset of the local IP subnet. Since the IP addresses
would normally be allocated on an FCFS basis, use docker compose to set it explicitly.

Jan 2024 changed macvlan to ipvlan as allegedly more efficient and performant.

## Customizing containers

The homebridge image currently lacks support for RPi GPIO so first build the
`townsen/homebridge` container:

    docker build -f Dockerfile.homebridge -t townsen/homebridge .

### Bring up the Services

This command will bring the containers up in the background. They will restart on reboot.

    docker-compose up --detach

## Making Changes

After changing any Dockerfile parameters, or to fetch the latest images run the above
command again:

    docker-compose up -d

## Advertize a `.local` address

To make the hard-coded IP more user friendly, advertize it using mDNS.

In the `/etc/avahi-daemon/hosts` file add a line for each service:

    codewiki.local 192.168.0.241

Note that if you use the `hostname` directive in the service, _and_ you run `avahi` in the
container (as [homebridge](homebridge.io) does) then this step is not necessary. However,
the host running the macvlan network cannot exchange traffic with the containers on the
network, so the advertisement will not be found on the host. Even if it was, it would not
connect. This can be fixed using a [shim
network](https://blog.oddbit.com/post/2018-03-12-using-docker-macvlan-networks/)


