#!/bin/bash
# Note the hardware acceleration devices!
# They were optional but koush removed them on 23/01/2022 
# I added all of them (originally he had the first three) 
#
docker run \
  -d --restart unless-stopped \
  --name=scrypted \
  --memory=2GB \
  --network host \
  --device=/dev/video10 \
  --device=/dev/video11 \
  --device=/dev/video12 \
  --device=/dev/video13 \
  --device=/dev/video14 \
  --device=/dev/video15 \
  --device=/dev/video16 \
  -v /srv/scrypted:/server/volume \
  koush/scrypted
