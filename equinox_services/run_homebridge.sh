#!/bin/bash
#
# Start Homebridge in a container
# Notes:
# - the config files are owned by townsen (1606) so set PUID
# - the i2c device bus is owned by root and group i2c (124) so set PGID
# - There were no issues with the homebridge:latest (ie. Alpine based)
#   container but I used the homebridge:ubuntu container in case...
#
docker run -d --name homebridge --restart=unless-stopped --device=/dev/i2c-1 --net=host -e PGID=124 -e PUID=1606 -v /srv/homebridge:/homebridge -e TZ=Europe/London oznu/homebridge:ubuntu
