#!/bin/bash
# Start the CodeWiki
# Use the regular HTTP port as we have no other webservers
#
docker run -d --restart=unless-stopped -v /srv/code.wiki:/wiki -p 80:4567 --name gollum townsen/gollum:rpi
