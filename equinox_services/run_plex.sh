#!/bin/bash
#
# Clone: git@github.com:plexinc/pms-docker
# Build: docker build -t plexinc/pms-docker:latest -f Dockerfile.arm64 . 
#
# Sign in online at plex.tv then request a claim code: https://plex.tv/claim
# Then when you run the client app it will automatically find the server and
# you can configure it.
#
docker run \
-d \
--name plex \
--restart=unless-stopped \
--network=host \
-e TZ="Europe/London" \
-e PLEX_CLAIM="claim-QS2LnNed8iLQf4c5WQe3" \
-e PLEX_UID=1606 \
-e PLEX_GID=1606 \
-v /srv/plex/config:/config \
-v /srv/plex/transcode:/transcode \
-v /raid/samba/Movies:/data \
plexinc/pms-docker
