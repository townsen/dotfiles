# My Setup

Reasons why I setup my system this way - so I don't forget.

## Ruby
After initially using RVM I decided to switch to chruby and ruby-install. Articles [like
this](https://stevemarshall.com/journal/why-i-use-chruby/) and
[this](https://ryanbigg.com/2014/10/ubuntu-ruby-ruby-install-chruby-and-you) amongst others were
motivations.

However, I noticed that I'd detoured into using 'rbenv' at one point, and couldn't
remember why.

### Installation

When installing a new ruby using `ruby-install` remember that the version of OpenSSL that
is installed by brew is keg only, to avoid messing with the MacOS version. Ruby installs
will fail with OpenSSL errors if you don't follow this advice:

> openssl@1.1 is keg-only, which means it was not symlinked into /usr/local,
> because openssl/libressl is provided by macOS so don't link an incompatible version.
>
> If you need to have openssl@1.1 first in your PATH run:
>   echo 'export PATH="/usr/local/opt/openssl@1.1/bin:$PATH"' >> ~/.bash_profile
>
> For compilers to find openssl@1.1 you may need to set:
>   export LDFLAGS="-L/usr/local/opt/openssl@1.1/lib"
>   export CPPFLAGS="-I/usr/local/opt/openssl@1.1/include"

### Gotchas

The Ruby version you select with `chruby` will only persist if you create a `.ruby-version` file. If you are inadvertently in the wrong ruby you'll get messages like this:

```bash
🍏 draco % bundle exec rake -T                                                                (master)draco
Could not find rake-13.0.1 in any of the sources
Run `bundle install` to install missing gems.
```

## TODO

I got a helpful message when pushing to GitLab saying it supports LFS locking and that I
should execute the following command in the repo:

    git config lfs.https://gitlab.com/townsen/dotfiles.git/info/lfs.locksverify true
